﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoviesBlog.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int PostID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Content { get; set; }
        public virtual Post Post { get; set; }
    }
}