﻿namespace MoviesBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PostID = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Author = c.String(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Post", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Post",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Author = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        MovieId = c.Int(),
                        Category = c.Int(nullable: false),
                        Content = c.String(),
                        ImagePath = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Movie", t => t.MovieId)
                .Index(t => t.MovieId);
            
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Genre = c.Int(nullable: false),
                        ReleaseDate = c.DateTime(nullable: false),
                        Producer = c.String(nullable: false),
                        RunningTime = c.Int(nullable: false),
                        Rate = c.Double(nullable: false),
                        Language = c.Int(nullable: false),
                        ImageName = c.String(),
                        Location_Longtitude = c.Double(nullable: false),
                        Location_Latitude = c.Double(nullable: false),
                        TrailerPath = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Map",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Location_Longtitude = c.Double(nullable: false),
                        Location_Latitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 128),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Post", "MovieId", "dbo.Movie");
            DropForeignKey("dbo.Comment", "PostID", "dbo.Post");
            DropIndex("dbo.Post", new[] { "MovieId" });
            DropIndex("dbo.Comment", new[] { "PostID" });
            DropTable("dbo.User");
            DropTable("dbo.Map");
            DropTable("dbo.Movie");
            DropTable("dbo.Post");
            DropTable("dbo.Comment");
        }
    }
}
