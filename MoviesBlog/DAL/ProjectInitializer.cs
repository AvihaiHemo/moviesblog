﻿using MoviesBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoviesBlog.DAL
{
    public class ProjectInitializer : System.Data.Entity.DropCreateDatabaseAlways<ProjectContext>

    {
        protected override void Seed(ProjectContext context)
        {

            var movies = new List<Movie>
            {
                new Movie
                {
                    Id=1,
                    Name="Titanic",
                    Genre=Genre.Drama,
                    ReleaseDate=new DateTime(1997, 11 , 1),
                    Producer="Twentieth Century Fox",
                    RunningTime=194,
                    Language=Models.Language.English,
                    Location=new Coordinate { Longtitude=50, Latitude=1 },
                    ImageName="titanic-the-movie.jpg",
                     TrailerPath="-iRajLSA8TA"
                },
                new Movie
                {
                    Id=2,
                    Name="Shrek",
                    Genre=Genre.Animated,
                    ReleaseDate=new DateTime(2001, 4, 22),
                    Producer="Universal",
                    RunningTime=90,
                    Rate=7.9,
                    Language=Models.Language.English,
                    ImageName="shrek.jpg",
                    Location=new Coordinate { Longtitude=-81.3, Latitude=28.55 },
                    TrailerPath="W37DlG1i61s"
                },
                new Movie
                {
                    Id=3,
                    Name="Toy Story",
                    Genre=Genre.Animated,
                    ReleaseDate=new DateTime(1995, 11, 19),
                    Producer="Pixar",
                    RunningTime=81,
                    ImageName="Toy_Story.jpg",
                    Language=Models.Language.English,
                    Location=new Coordinate { Longtitude=-81.35, Latitude=28.5 },
                    TrailerPath="KYz2wyBy3kc"
                },
                new Movie
                {
                    Id=4,
                    Name="Pretty Woman",
                    Genre=Genre.Drama,
                    ReleaseDate=new DateTime(1990, 3, 23),
                    Producer="Touchston Pictures",
                    RunningTime=119,
                    Language=Models.Language.English,
                    ImageName="Prettty-Woman.jpg",
                    Location=new Coordinate { Longtitude=-109.13, Latitude=36.17},
                    TrailerPath="Wzii8IuL8lk"
                },
                new Movie
                {
                    Id=5,
                    Name="The Hangover",
                    Genre=Genre.Comedy,
                    ReleaseDate=new DateTime(2009, 5, 30),
                    Producer="Warner Bros",
                    RunningTime=100,
                    Language=Models.Language.English,
                    ImageName="hangover.jpg",
                    Location=new Coordinate { Longtitude=-115.13, Latitude=36.17 },
                     TrailerPath="tcdUhdOlz9M"
                },
               new Movie
                {
                    Id=6,
                    Name="Se La Vi",
                    Genre=Genre.Drama,
                    ReleaseDate=DateTime.Now,
                    Producer="Quad Prosunction",
                    RunningTime=117,
                    ImageName="ce-la-vi.jpg",
                    Language=Models.Language.France,
                    Location=new Coordinate { Longtitude=5, Latitude=45 },
                     TrailerPath="WjV8m84FcOs"
                },
                new Movie
                {
                    Id=7,
                    Name="Fast and Furious",
                    Genre=Genre.Action,
                    ReleaseDate=new DateTime(2001, 6, 18),
                    Producer="Universal Pictures",
                    RunningTime=106,
                    Language=Models.Language.English,
                    ImageName="Fast_and_Furious.jpg",
                    Location=new Coordinate { Longtitude=-115.13, Latitude=42},
                    TrailerPath="2TAOizOnNPo"
                },
                 new Movie
                {
                    Id=8,
                    Name="2 fast 2 furious",
                    Genre=Genre.Drama,
                    ReleaseDate=new DateTime(2003, 6, 3),
                    Producer="Universal Pictures",
                    RunningTime=107,
                    ImageName="Two_fast_two_furious.jpg",
                    Language=Models.Language.English,
                    Location=new Coordinate { Longtitude=-108.13, Latitude=47 },
                    TrailerPath="F_VIM03DXWI"
                },
                new Movie
                {
                    Id=9,
                    Name="Inception",
                    Genre=Genre.ScienceFiction,
                    ReleaseDate=new DateTime(2010, 7, 8),
                    Producer="Warner Bros",
                    RunningTime=148,
                    ImageName="inception.png",
                    Language=Models.Language.English,
                    Location=new Coordinate { Longtitude=-75, Latitude=45 },
                    TrailerPath="YoHD9XEInc0"
                },
                new Movie
                {
                    Id=10,
                    Name="Solo a Star Wars Story",
                    Genre=Genre.Drama,
                    ReleaseDate=new DateTime(2019, 5, 10),
                    Producer="Walt Disney Pictures",
                    RunningTime=135,
                    ImageName="Solo_A_Star_Wars_Story_poster.jpg",
                    Language=Models.Language.English,
                    Location=new Coordinate { Longtitude=-120.13, Latitude=44 },
                    TrailerPath="jPEYpryMp2s"
                }
            };

            movies.ForEach(h => context.Movies.Add(h));
            context.SaveChanges();

            var posts = new List<Post>
            {
                new Post {
                    Id = 1,
                    Title = "Why did they have to kill Leonardo",
                    Author = "aaaa",
                    Date=new DateTime(2019, 9, 10),
                    Content="I believe We all would have preferred Jack Dawson to live, even if it was Rose's death",
                    Category = Category.Celebs,
                    ImagePath="kate_winslet_jack_titanic_.jpg",
                    MovieId=movies[0].Id
                },
                new Post  {
                    Id = 2,
                    Title = "More frightening than hell",
                    Author="aaaa",
                    Date=new DateTime(2019,5,2),
                    Content="As a person who loves horror movies, I admit it too, Annabelle has crossed every boundary - the most terrifying movie I have ever seen!",
                    Category = Category.Reviews,
                    MovieId =movies[8].Id
                },
                new Post  {
                    Id = 3,
                    Title = "When part B is better then part A",
                    Author="Tom",
                    Date=new DateTime(2019,9,30),
                    Content="Even though the first 'fast and furious' was an excellent film, I must admit that the sequel was even better",
                    Category = Category.NewMovies,
                    ImagePath="fast-furious6.jpg",
                    MovieId =movies[7].Id
                },
                new Post  {
                    Id = 4,
                    Title = "When comedy and horror connect",
                    Author="Guest",
                    Date=new DateTime(2019,8,26),
                    Content="A great movie with great actors - I've never been so nervous from a comedy movie",
                    Category = Category.NewsFlash,
                    MovieId=movies[5].Id
                },
                new Post  {
                    Id = 5,
                    Title = "Best movie i have ever seen",
                    Author="bb cc",
                    Date=new DateTime(2019,8,21),
                    Content="'Shrek' is by far the best movie ever made - I wish I could trade with Fiona",
                    Category = Category.Reviews,
                    MovieId =movies[1].Id
                }
            };

            posts.ForEach(p => context.Posts.Add(p));
            context.SaveChanges();

            var comments = new List<Comment>
            {
                new Comment
                {
                    Id=1,
                    Title="True and accurate!",
                    Author="user123",
                    Content="Agree with every word",
                    PostID=1
                },
                new Comment
                {
                    PostID=2,
                    Id=2,
                    Title="I couldnt Agree more",
                    Author="anonymus",
                    Content="Rose is the wrost",
                },
                new Comment
                {
                    PostID=3,
                    Id=3,
                    Title="Nonsense",
                    Author="BestGuy94",
                    Content="This movie is not scarry at all",
                },
                new Comment
                {
                    PostID=4,
                    Id=4,
                    Title="cmms",
                    Author="beast%^&",
                    Content="Agree with every word you write! I choose movies only according to your reviews!",
                },
            };
            comments.ForEach(c => context.Comments.Add(c));
            context.SaveChanges();

            var users = new List<User>
            {
                new User
                {
                    UserName="Avihai Hemo",
                    Password="Avihai111"
                }
            };

            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();


            var maps = new List<Map>();

            for (int i = 0; i < movies.ToArray().Length; i++)
            {
                Map map = new Map
                {
                    Id = movies[i].Id,
                    Name = movies[i].Name,
                    Location = movies[i].Location
                };

                maps.Add(map);
            }
            maps.ForEach(m => context.Maps.Add(m));
            context.SaveChanges();
            base.Seed(context);
        }
    }
}